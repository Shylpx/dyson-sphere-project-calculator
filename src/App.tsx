import Navbar from './navigation/Navbar'
import Calculator from './core/Calculator'
import './App.css'

import { ThemeProvider, createTheme } from '@mui/material/styles'

const theme = createTheme({
  typography: {
    fontSize: 14,
    fontFamily: 'Saira',
    fontWeightRegular: 400
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Navbar/>
        <Calculator/>
      </ThemeProvider>
    </div>
  );
}

export default App
