import AppBar from '@mui/material/AppBar'
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'

export default function Navbar() {

  return (
    <Box>
      <AppBar
        position="static"
        variant="outlined"
        elevation={0}
      >
        <Typography
          variant="h4"
          component="div"
          sx={{
            mx: 4,
            my: 2,
            fontFamily: 'Saira',
            fontWeight: 400
          }}
        >
          Dyson Sphere Program Calculator
        </Typography>
      </AppBar>
    </Box>
  )
}
