import React, { useState, useMemo, useReducer } from 'react'
import ManufacturingTree from './ManufacturingTree'
import productData from '../data/products.json'
import machineData from '../data/machines.json'

import '../style/Calculator.css'

import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import FormControl from '@mui/material/FormControl'
import Autocomplete from '@mui/material/Autocomplete'
import Select, { SelectChangeEvent } from '@mui/material/Select'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import InputLabel from '@mui/material/InputLabel'
import InputAdornment from '@mui/material/InputAdornment'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

const DEFAULT_AMOUNT = 30 // per second
const TAB_LIST = [
  'Machines',
  'Recipes',
  'Proliferator'
]

export default function Calculator() {
  const outputProductNames = useMemo(() => {
    return productData.items.filter(item => !item.raw_material).map(item => item.name)
  }, [])

  const [product, setProduct] = useState<string | null>(null)
  const [amount, setAmount] = useState<number>(DEFAULT_AMOUNT)
  const [machineSpeed, updateMachineSpeed] = useReducer(
    (state: Record<string, number>, action: { type: string, value: number }) => {
      return {
        ...state,
        [action.type]: action.value
      }
    },
    machineData.reduce((list, machine) => ({ ...list, [machine.class]: machine.types[0].speed }), {})
  )
  const [selectedSettingTabIndex, setSettingTabIndex] = useState<number>(0)

  function handleProductChange(_event: React.SyntheticEvent, value: string | null) {
    setProduct(value)
  }

  function handleAmountChange(event: React.ChangeEvent<HTMLInputElement>) {
    setAmount(Math.max(Number(event.target.value), 1))
  }

  function handleMachineChange(event: SelectChangeEvent<number>) {
    updateMachineSpeed({ type: event.target.name, value: Number(event.target.value) })
  }

  function handleTabChange(_event: React.SyntheticEvent, value: number) {
    setSettingTabIndex(value)
  }

  return (
    <Box m={4}>
      <Paper elevation={3}>
        <Box p={3}>
          <Grid container alignItems="center" spacing={4}>
            <Grid item xs={12} md={4}>
              <Typography variant="h6" component="div" sx={{ mb: 2 }}>
                What do you <span className='lineThrough'>want</span> need to produce?
              </Typography>
              <TextField
                type="number"
                size="small"
                fullWidth
                value={amount}
                onChange={handleAmountChange}
                InputProps={{
                  endAdornment: <InputAdornment position="end">per second</InputAdornment>
                }}
              />
              <Autocomplete
                size="small"
                fullWidth
                options={outputProductNames}
                value={product}
                onChange={handleProductChange}
                renderInput={(params) => <TextField {...params}/>}
              />
            </Grid>
            <Grid item xs={12} md={8}>
              <Paper elevation={3}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  <Tabs
                    variant="fullWidth"
                    value={selectedSettingTabIndex}
                    onChange={handleTabChange}
                  >
                    {TAB_LIST.map(name => <Tab key={name} label={name}/>)}
                  </Tabs>
                </Box>
                <Box p={2}>
                  {selectedSettingTabIndex === 0
                    ? machineData.map((machine) => {
                        return (
                          <Box key={machine.class} sx={{ p: 1 }}>
                            <FormControl fullWidth sx={{ maxWidth: 600 }}>
                              <InputLabel>{machine.class}</InputLabel>
                              <Select
                                name={machine.class}
                                label={machine.class}
                                size="small"
                                value={machineSpeed[machine.class]}
                                onChange={handleMachineChange}
                                disabled={machine.types.length <= 1}
                              >
                                {machine.types.map((type) => {
                                  return <MenuItem key={type.name} value={type.speed}>{`${type.name} (x${type.speed})`}</MenuItem>
                                })}
                              </Select>
                            </FormControl>
                          </Box>
                        )
                      })
                    : ''
                  }
                </Box>
              </Paper>
            </Grid>
          </Grid>
        </Box>
      </Paper>
      <ManufacturingTree product={product} amount={amount} machineSpeed={machineSpeed} />
    </Box>
  );
}
