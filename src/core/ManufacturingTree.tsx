import { useState, useMemo } from 'react'
import products from '../data/products.json'
import ManufacturingTotal from './ManufacturingTotal'

import '../style/Calculator.css'

import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Chip from '@mui/material/Chip'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

import KeyboardDoubleArrowRightOutlinedIcon from '@mui/icons-material/KeyboardDoubleArrowRightOutlined'
import SubdirectoryArrowRightOutlinedIcon from '@mui/icons-material/SubdirectoryArrowRightOutlined'

const TAB_LIST = [
  'Production tree',
  'Totals'
]

export type ManufacturingUnit = {
  name: string,
  amount: number,
  machineType?: string,
  machineAmount?: number
}

export type NodeInfo = ManufacturingUnit & {
  input?: NodeInfo[]
}

export default function ManufacturingTree(
  { product, amount, machineSpeed }:
  { product: string | null, amount: number, machineSpeed: Record<string, number> }
) {
  const [selectedSettingTabIndex, setSettingTabIndex] = useState<number>(0)

  const root = useMemo<NodeInfo | null>(() => {
    if (!product) {
      return null
    }

    function calculateNodeInfo(
      productName: string,
      requiredAmount: number
    ): NodeInfo {
      const productData = products.items.find(item => item.name === productName)
      if (!productData) {
        throw Error(`Product not found: ${productName}`)
      }

      const isRawMaterial = productData.raw_material

      let recipe: typeof products.recipes[number] | undefined
      if (!isRawMaterial) {
        recipe = products.recipes.find(r => r.output === productName)
      }

      // Node calculation
      let machineAmount: number | undefined, inputNodes: NodeInfo[] = []
      if (!isRawMaterial) {
        if (!recipe) {
          throw Error('Recipe not found')
        }

        const productionTime = recipe.time / machineSpeed[recipe.machine]
        machineAmount = requiredAmount * productionTime / recipe.amount

        inputNodes = recipe.input.map(inputItem => {
          return calculateNodeInfo(
            inputItem.name,
            machineAmount! * inputItem.amount / productionTime
          )
        })
      }

      return {
        name: productName,
        amount: requiredAmount,
        ...(isRawMaterial ? {} : {
          machineType: recipe!.machine,
          machineAmount: machineAmount!,
          input: inputNodes!
        })
      }
    }

    return calculateNodeInfo(product, amount)
  }, [product, amount, machineSpeed])

  function handleTabChange(_event: React.SyntheticEvent, value: number) {
    setSettingTabIndex(value)
  }

  function renderNode(nodeInfo: NodeInfo, Icon = KeyboardDoubleArrowRightOutlinedIcon) {
    return (
      <Box pt={0.5} textAlign="left" key={nodeInfo.name}>
        <Box>
          <Icon sx={{ verticalAlign: 'middle', my: 1 }} />
          <Chip sx={{ mr: 1 }} className="chip" label={nodeInfo.name} color="primary" />
          <Chip sx={{ ml: 1 }} className="chip" label={`${nodeInfo.amount}/s`} variant="outlined" color="secondary" />
          {
            nodeInfo.machineAmount
              ? <Chip sx={{ ml: 1 }} className="chip" label={`${nodeInfo.machineAmount} ${nodeInfo.machineType}`} variant="outlined" />
              : ''
          }
        </Box>
        {nodeInfo.input ? <Box ml={5}>{nodeInfo.input?.map(input => renderNode(input, SubdirectoryArrowRightOutlinedIcon))}</Box> : null}
      </Box>
    )
  }

  if (!root) {
    return null
  }
  return (
    <>
      <Paper elevation={3} sx={{ m: 3 }}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            variant="fullWidth"
            value={selectedSettingTabIndex}
            onChange={handleTabChange}
          >
            {TAB_LIST.map(name => <Tab key={name} label={name}/>)}
          </Tabs>
        </Box>
        <Box p={3}>
          { selectedSettingTabIndex === 0 ? renderNode(root) : '' }
          {
            selectedSettingTabIndex === 1
              ? <ManufacturingTotal rootNode={root} />
              : ''
          }
        </Box>
      </Paper>
    </>
  )
}
