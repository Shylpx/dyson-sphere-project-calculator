import { useMemo, useCallback } from 'react'
import { NodeInfo, ManufacturingUnit } from './ManufacturingTree'

import '../style/Calculator.css'

import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import Chip from '@mui/material/Chip'

import KeyboardDoubleArrowRightOutlinedIcon from '@mui/icons-material/KeyboardDoubleArrowRightOutlined'
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

type Total = ManufacturingUnit[]

export default function ManufacturingTotal({ rootNode }: { rootNode: NodeInfo}) {
  const calculateTotal = useCallback((node: NodeInfo) => {
    const { name, amount, machineType, machineAmount } = node

    if (!node.input) {
      return [{ name, amount, machineType, machineAmount }]
    }

    const unitMap: Record<string, ManufacturingUnit> = {
      [name]: { name, amount, machineType, machineAmount }
    }
    node.input.map(inputNode => calculateTotal(inputNode)).flat().forEach(unit => {
      if (unit.name in unitMap) {
        unitMap[unit.name].amount += unit.amount
        if (unit.machineAmount) {
          unitMap[unit.name].machineAmount =
            (unitMap[unit.name].machineAmount || 0) + unit.machineAmount
        }
      } else {
        unitMap[unit.name] = unit
      }
    })

    return Object.values(unitMap)
  }, [])

  const total = useMemo<Total>(() => {
    return calculateTotal(rootNode)
  }, [calculateTotal, rootNode])

  if (!total) {
    return null
  }
  return (
    <Box textAlign="left">
      <Grid container alignItems="top" spacing={4}>
        <Grid item xs={12} md={6}>
          <Typography variant="h6">Production</Typography>
          <Box>
            {total.filter(unit => unit.machineAmount).sort(
              (unitA, unitB) => unitB.machineAmount! - unitA.machineAmount!
            ).map(unit => (
              <Box key={unit.name} my={1} >
                <KeyboardDoubleArrowRightOutlinedIcon sx={{ verticalAlign: 'middle' }} />
                <Chip sx={{ ml: 1, mt: 0.5 }} className="chip" label={`${unit.amount} / s`} color="secondary" />
                <Chip sx={{ ml: 1, mt: 0.5 }} className="chip" label={unit.name} color="primary" />
                <ArrowRightAltIcon sx={{ verticalAlign: 'middle', ml: 1 }} />
                <Chip sx={{ ml: 1, mt: 0.5 }} className="chip" label={
                  <span><strong>{unit.machineAmount}</strong> {unit.machineType}</span>
                } />
              </Box>
            ))}
          </Box>
        </Grid>
        <Grid item xs={12} md={6}>
          <Typography variant="h6">Raw materials</Typography>
          <Box>
            {total.filter(unit => !unit.machineAmount).sort(
              (unitA, unitB) => unitB.amount - unitA.amount
            ).map(unit => (
              <Box key={unit.name} my={1} >
                <KeyboardDoubleArrowRightOutlinedIcon sx={{ verticalAlign: 'middle' }} />
                <Chip sx={{ ml: 1, mt: 0.5 }} className="chip" label={`${unit.amount} / s`} color="secondary" />
                <Chip sx={{ ml: 1, mt: 0.5 }} className="chip" label={unit.name} color="primary" />
              </Box>
            ))}
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}
